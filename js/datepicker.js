function initDatepicker(month, year) {
	
	if(month == null && year == null) {
		if($(".selectedDate").val()) {
			var arr = $(".selectedDate").val().split(",");
			var minimum = Math.min.apply(Math, arr).toString();
			var date = new Date(minimum.substring(0, 4), minimum.substring(4, 6) - 1, 1);
		}
		else {
			var date = new Date();
		}
	}
	else {
		var date = new Date(year, month, 1);
		
	}
	var month = date.getMonth();
	var year = date.getFullYear();
	var today = new Date();
	
	var months = new Array("Januar", "Februar", "März", "April", "Mai", "Juni", "Juli", "August", "September", "Oktober", "November", "Dezember");
	
	
	
	var daysInMonth = 31;
	if(month == 3 || month == 5 || month == 8 || month == 10) daysInMonth = 30;
	if(month == 1) {
		if(year % 4 == 0) daysInMonth = 29;
		else daysInMonth = 28;
	}
	
	$(".datepicker .nav .month").text();
	var firstDate = date;
	firstDate.setDate(1);
	var firstDateDay = firstDate.getDay();
	firstDateDay = (firstDateDay == 0) ? 7: firstDateDay;
	
	var prevMonth = month - 1;
	var nextMonth = month + 1;
	var prevYear = year;
	var nextYear = year;
	if(nextMonth == 12) {
		nextMonth = 0;
		nextYear = year + 1;
	}
	if(prevMonth == -1) {
		prevMonth = 11;
		prevYear = year - 1;
	}
	
	var counter = 2 - firstDateDay;
	
	$(".datepicker").html("<div><div style=\"float: left;\"><a style=\"padding: 4px;\" onclick=\"initDatepicker("+prevMonth+", "+prevYear+")\">◀</a></div><div style=\"float: right;\"><a style=\"padding: 4px;\" onclick=\"initDatepicker("+nextMonth+", "+nextYear+")\">▶</a></div><div style=\"text-align: center; cursor: pointer;\" title=\"Heute\" onclick=\"initDatepicker("+today.getMonth()+", "+today.getFullYear()+")\">"+months[month]+" "+year+"</div></div></div><table><tr><th>Mo</th><th>Di</th><th>Mi</th><th>Do</th><th>Fr</th><th>Sa</th><th>So</th></tr><tr><td></td><td></td><td></td><td></td><td></td><td></td><td></td></tr><tr><td></td><td></td><td></td><td></td><td></td><td></td><td></td></tr><tr><td></td><td></td><td></td><td></td><td></td><td></td><td></td></tr><tr><td></td><td></td><td></td><td></td><td></td><td></td><td></td></tr><tr><td></td><td></td><td></td><td></td><td></td><td></td><td></td></tr><tr><td></td><td></td><td></td><td></td><td></td><td></td><td></td></tr></table>");
	
	month = month + 1;
	month = month.toString();
	if(month.length == 1) month = "0" + month;
	
	
	$(".datepicker table td").each(function() {
		if(counter <= daysInMonth && counter >= 1) {
			
			var day = counter.toString();
			if(day.length == 1) day = "0" + day;
			var date = year + "" + month + "" + day;
			
			$(this).text(counter);
			$(this).addClass("selectable");
			$(this).attr("id", date);
			if($(".selectedDate").val().indexOf(date) > -1) {
				$(this).attr("onclick", "removeDate("+date+")");
				$(this).addClass("selected");
			}
			else {
				$(this).attr("onclick", "addDate("+date+")");
			}
			if(today.getDate() == day && today.getMonth() + 1 == month && today.getFullYear() == year) $(this).addClass("today");
		}
		else {
			$(this).css("border", 0);
		}
		counter++;
	});
}

function addDate(date) {
	var elem = $("#"+date);
	if(!$(".selectedDate").val())
		$(".selectedDate").val($(".selectedDate").val()+date);
	else
		$(".selectedDate").val($(".selectedDate").val()+","+date);
	elem.addClass("selected");
	elem.attr("onclick", "removeDate("+date+")");
}

function removeDate(date) {
	var elem = $("#"+date);
	var oldString = $(".selectedDate").val();
	var newString = oldString.replace(date, "");
	newString = newString.replace(",,", ",");
	if(newString.charAt(0) == ",")
		newString = newString.substring(1);
	if(newString.charAt(newString.length - 1) == ",")
		newString = newString.substring(0, newString.length - 1);
	$(".selectedDate").val(newString);
	elem.removeClass("selected");
	elem.attr("onclick", "addDate("+date+")");
}

$(document).ready(function() {
	initDatepicker();
});